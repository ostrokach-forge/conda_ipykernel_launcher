#!/bin/bash

set -ev

mkdir -p ${PREFIX}/bin

cat <<EOF > ${PREFIX}/bin/conda_ipykernel_launcher.sh
#!/bin/bash

set -ev

source ${PREFIX}/bin/activate

python -m ipykernel_launcher -f \${1}

EOF

chmod +x ${PREFIX}/bin/conda_ipykernel_launcher.sh
